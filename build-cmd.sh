#!/bin/bash

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

TOP="$(dirname "$0")"

DB_VERSION="5.1.25"
DB_SOURCE_DIR="db-$DB_VERSION"


if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# load autbuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

stage="$(pwd)"
case "$AUTOBUILD_PLATFORM" in
    "linux64")
        pushd "$TOP/$DB_SOURCE_DIR/build_unix"
            LDFLAGS="-m64" CFLAGS="-m64" CXXFLAGS="-m64" ../dist/configure --prefix="$stage" --target=x86_64-linux-gnu
            make
            make install
        popd
        mv lib release
        mkdir -p lib
        mv release lib
    ;;
    *)
        exit -1
    ;;
esac


mkdir -p "$stage/LICENSES"
cp "$TOP/$DB_SOURCE_DIR/LICENSE" "$stage/LICENSES/db.txt"
echo "$DB_VERSION" > "$stage/VERSION.txt"

pass

